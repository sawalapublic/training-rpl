<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Recaptcha
 *
 * Handler untuk recaptcha
 */

class Zenziva {

    var $endpoint       = "https://reguler.zenziva.net/apps/smsapi.php";
    var $userKey        = "t2qghl";
    var $passKey        = "5mwnmd21r9";

    /**
     * Send SMS
     *
     * @param $nohp
     * @param $pesan
     * @return mixed
     */
    public function send($nohp,$pesan){

        $data = array(
          'userkey'   => $this->userKey,
          'passkey'   => $this->passKey,
          'nohp' => $nohp,
          'pesan' => $pesan
        );

        $curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, $this->endpoint);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curlHandle, CURLOPT_HEADER, 0);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
        curl_setopt($curlHandle, CURLOPT_POST, 1);
        $results = curl_exec($curlHandle);
        curl_close($curlHandle);

        $XMLdata = new SimpleXMLElement($results);
        return $XMLdata;

    }

}