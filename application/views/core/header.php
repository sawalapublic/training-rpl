<!doctype html>
<html lang="en">
<head>


    <title><?=$this->config->item('application_name');?></title>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="<?=base_url()?>resources/style.css">
    <link rel="stylesheet" href="<?=base_url()?>resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>resources/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=base_url()?>resources/datatables/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?=base_url()?>resources/css/main.css?v=1.1">
    <link rel="stylesheet" href="<?=base_url()?>resources/select2/dist/css/select2.min.css">
</head>
<body>
<input type="hidden" id="base_url" value="<?=base_url()?>">

