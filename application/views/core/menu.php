<nav class="navbar fixed-top navbar-expand-lg">

        <a class="navbar-brand" href="#" alt="<?=$this->config->item('application_name');?>">
            <img class="logo" src="<?=base_url()?>resources/img/logo.jpeg">
        </a>

        <button class="navbar-toggler" id="bg-light" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
            <span class="navbar-toggler-icon" style="padding: 1px;"><i class="fa fa-bars" style="font-size: 30px; color: #ffa826"></i></span>
        </button>


        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto" style="margin-right: 40px;">
                <?php if ($_SESSION['status'] == 0) {
                            ?>
                          <li class="nav-item ">
                            <a href="<?=base_url('dudi/DudiList/get')?>">Daftar Instansi</a>
                          </li>
                          <li class="nav-item dropdown">
                               <a class="dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                  Data Siswa
                               </a>
                                <div class="dropdown-menu">
                                  <a class="dropdown-item" href="<?=base_url('siswa/SiswaList/get')?>">Siswa Keseluruhan</a>
                                  <a class="dropdown-item" href="<?=base_url('siswa/SiswaListPkl/getsemua')?>">Siswa Siap PKL</a>
                                </div>
                          </li>
                          <?php
                        }
                        ?>
                          <li class="nav-item dropdown">
                               <a class="dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                  Pengguna
                               </a>
                                <div class="dropdown-menu">
                                  <p class="dropdown-item"><?php echo $_SESSION['username']; ?></p>
                                  <p class="dropdown-item"><?php if ($_SESSION['status'] == 0) {
                                    echo "Admin";
                                  }elseif ($_SESSION['status'] == 1) {
                                    echo "Siswa";
                                  } ?></p>
                                  <div class="dropdown-divider"></div>
                                  <a class="dropdown-item" href="<?=base_url('user/User/logout')?>">Keluar</a>
                                </div>
                          </li>
            </ul>

        </div>
</nav>
