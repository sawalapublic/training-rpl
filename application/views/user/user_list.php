<section style="margin-top: 30px;">
    <div class="container">

        <div class="row justify-content-md-center">
            <div class="col-lg-12">


                <div class="row" style="margin-bottom: 0px;">
                    <div class="col-lg-6">
                        <h4 style="margin-bottom: 0px; color: #1a374a;">Data Pendaftar</h4>
                    </div>
                    <div class="col-lg-6">
                        <a href="<?=base_url("user/User")?>" style="margin-bottom: 10px;" class="pull-right "><button class="btn btn-primary">Lihat Pendaftar</button></a>
                    </div>
                </div>
                <hr style="margin-top: 0px;"><br>

                <div class="row">
                    <div class="col-12 col-md-6 col-lg-12">
                        <table class="datatablesGeneral table table-responsive-md table-bordered" cellspacing="0" width="100%">
                            <thead id="bg-dark">
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Profil</th>
                                <th>Tanggal Lahir</th>
                                <th>Jenis Kelamin</th>
                                <th>Alamat</th>
                                <th>Email</th>
                                <th>Handphone</th>
                            </tr>
                            </thead>
                            <tbody class="table-hover">
                                <?php $no=1; foreach ($user as $row): ?>
                                <tr>
                                    <td><?php echo $no;?></td>
                                    <td><?=$row['nama']?></td>
                                    <td style="padding: 10px;"><img src="<?=$row['file']?>" style="width: 100%; height: auto;"></td>
                                    <td><?=$row['tanggal_lahir']?></td>
                                    <td><?=$row['jk']?></td>
                                    <td><?=$row['alamat']?></td>
                                    <td><?=$row['email']?></td>
                                    <td><?=$row['handphone']?></td>
                                </tr>
                            <?php $no++; endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>