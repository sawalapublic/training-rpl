</body>
<footer id="footer">
    <div class="container-fluid" style="background: #152b3a;color:white;">
        <div class="row justify-content-center" style="padding: 10px;">
            <div class="col-11 col-md-3 col-lg-2">
                <p style="text-align: center; margin: 0px;">2019 © Training RPL</p>
            </div>
        </div>
    </div>
</footer>

<script src="<?=base_url()?>resources/js/jquery-3.2.1.min.js"></script>
<script src="<?=base_url()?>resources/js/popper.min.js"></script>
<script src="<?=base_url()?>resources/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>resources/datatables/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>resources/datatables/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url()?>resources/js/main.js?v=1.53"></script>
<script src="<?=base_url()?>resources/select2/dist/js/select2.min.js" ></script>
<script>
    $(document).ready(function() {
    $('.select2').select2();
});

    $('#notifications').slideDown('slow').delay(3000).slideUp('slow');


    </script>
</html>