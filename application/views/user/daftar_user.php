<section style="margin-top: 20px; margin-bottom: 100px;">
    <div class="container">
        <form  action="<?php echo base_url()."user/User/add_akun"; ?>" method="post" enctype="multipart/form-data" >
            <div class="row justify-content-md-center">
                <div class="col-lg-12" style="text-align: center;">
                    <h1>Form Pendaftaran</h1>
                </div>
            </div>

            <div class="row justify-content-md-center" style="margin-top: 20px;">
                <div class="col-lg-7">
                    <div class="form-group">
                        <label>Nama <span class="text-danger">*</span></label>
                        <input value="" type="text" name="nama" class="form-control" required="">
                    </div>

                    <div class="form-group">
                        <label for="title">Foto Profil <span class="text-danger">*</span></label>
                        <input type="file" class="form-control" required name="file" accept="image/*, .pdf">
                        <small>Mohon lampirkan foto profil anda</small>
                    </div>

                    <div class="form-group">
                        <label>Tanggal Lahir <span class="text-danger">*</span></label>
                        <input value="" type="date" name="tanggal_lahir" class="form-control" required="">
                    </div>

                    <div class="form-group">
                        <label>Jenis Kelamin <span class="text-danger">*</span></label>
                        <select name="jk" class="form-control" required>
                            <option value="laki-laki" selected >Laki-laki</option>
                            <option value="Perempuan" >Perempuan</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Alamat <span class="text-danger">*</span></label>
                        <textarea name="alamat" class="form-control" required></textarea>
                    </div>

                    <div class="form-group">
                        <label>Email <span class="text-danger">*</span></label>
                        <input value="" type="email" name="email" class="form-control" required="">
                    </div>

                    <div class="form-group">
                        <label>Handphone <span class="text-danger">*</span></label>
                        <input value="" type="number" name="handphone" class="form-control" required="">
                    </div>

                </div>
            </div>

            <br>

            <div class="row justify-content-md-center">
                <div class="col-lg-7">
                    <input type="submit" class="btn btn-primary btn-lg" value="Proses" style="background-color: #ffa826;">
                </div>
            </div>
        </form>

    </div>
</section>

                                