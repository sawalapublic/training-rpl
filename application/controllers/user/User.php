<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('user/User_model', 'userM');
        $this->load->library('zenziva');

    }

    public function index()
    {
        $this->helpM->header();
        $this->helpM->atas();

        $data_user = $this->userM->mydb->get();
        $data['user'] = $data_user;

        $this->load->view('user/user_list',$data);

        $this->helpM->footer();
    }

    public function add_akun()
    {
        $data_input = $this->input->post();
        unset($data_input['file']);

        $address = $data_input['email'];

        $subject ="New message from ".$data_input['nama'];


        $body  = "Pendaftaran telah diterima atas data berikut: </br>
                  From  : ".$data_input['nama']."<br/>
                  Email  : ".$data_input['email']."<br/>
                  No Mobile  : ".$data_input['handphone']."<br/>";

        $dataEmail = $this->packData($address,$subject,$body);
        $send = $this->sendPostmark($dataEmail);

        $send_sms = $this->sendSmsOrderReceived($data_input['handphone'],$data_input['nama']);

        $file = $_FILES['file']['tmp_name'];
        $nameUpload = $_FILES['file']['name'];
        $name = $nameUpload;
        $fileS3= $this->s3->inputFile($file);
        $putObject = $this->s3->putObject($fileS3,'training22juni',$name);

        // debug($file);

        if($putObject){

            $data['file'] = "https://s3-ap-southeast-1.amazonaws.com/training22juni/".$name;
        }else{
            $data['file'] = "";
        }

        $input = array(
            'nama' => $data_input['nama'],
            'file' => $data['file'],
            'tanggal_lahir' => $data_input['tanggal_lahir'],
            'jk' => $data_input['jk'],
            'alamat' => $data_input['alamat'],
            'email' => $data_input['email'],
            'handphone' => $data_input['handphone'],
        );
        $user = $this->userM->mydb->add($input);
        

        $this->helpM->header();
        $this->helpM->atas();

        $data_user = $this->userM->mydb->get();
        $data['user'] = $data_user;

        $this->load->view('user/user_list',$data);

        $this->helpM->footer();
        
        
    }

    public function packData($to,$subject,$body){
        $data = array(
            'From'    => "demo@formulir.id",
            'To'      => $to,
            'Subject' => $subject,
            'HtmlBody'=> $body
        );
        return json_encode($data);
    }

    public function sendPostmark($data)
    {

        $ch = curl_init();
        $curl_options = array(
            CURLOPT_URL             => "https://api.postmarkapp.com/email",
            CURLOPT_HTTPHEADER      => array(
                'Accept: application/json',
                'X-Postmark-Server-Token: 23984de1-546e-4d76-b0eb-2098fec45019',
                'Content-Type: application/json'
            ),
            CURLOPT_RETURNTRANSFER  => 1,
            CURLOPT_POST            => 1,
            CURLOPT_POSTFIELDS      => $data,
            CURLOPT_HTTPAUTH        => 1
        );

        curl_setopt_array($ch, $curl_options);
        $result = curl_exec($ch);
        return $result;

    }

    public function sendSmsOrderReceived($nomor,$nama){

        $msg = "Pendaftaran Telah diterima atas nama: ".$nama." (Training Ibis Hotel)";

        $sms = $this->zenziva->send($nomor,$msg);
    }

}